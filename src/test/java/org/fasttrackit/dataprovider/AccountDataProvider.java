package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.testng.annotations.DataProvider;

public class AccountDataProvider {

    @DataProvider(name = "AccountDataProvider")
    public static Object[][] createAccountProvider() {
        Account beetleAccount = new Account("beetle", "choochoo", "emailAddress");
        Account dinoAccount = new Account("dino", "choochoo", "emailAddress");
        Account turtleAccount = new Account("turtle", "choochoo", "emailAddress");

        return new Object[][]{
                {beetleAccount},
                {dinoAccount},
                {turtleAccount}
        };
    }
}
