package org.fasttrackit;

import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistPageTests {
    MainPage homepage = new MainPage();
    WishlistPage wishlistPage = new WishlistPage();

    @BeforeMethod
    public void setup(){
        homepage.clickOnTheLogoButton();
    }

    @AfterMethod
    public void returnToHomePage() {
        homepage.clickOnTheResetIcon();
    }


    @Test
    public void verify_the_wishlist_page_tittle_is_displayed() {
        homepage.clickOnTheWishlistButton();
        assertEquals(wishlistPage.getPageSubtitle(), "Wishlist", "Expected Wishlist page title to be: Wishlist");
        sleep(4000);
    }

    @Test
    public void verify_the_wishlist_page_contain_products() {
        Product p1 = new Product("1",new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToWishlist();
        Product p2 = new Product("2",  new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToWishlist();
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        p3.addProductToWishlist();
        homepage.clickOnTheWishlistButton();
        assertTrue(wishlistPage.validateProductsInWishlistAreDisplayed(),"Expected wishlist to contain products");
    }
}
