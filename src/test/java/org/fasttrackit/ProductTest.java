package org.fasttrackit;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class ProductTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "User can click on products test.",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_click_on_a_product(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = p.getExpectedResults().getTitle();
        String expectedPrice = p.getExpectedResults().getPrice();
        assertEquals(productPage.getTitle(), expectedTitle, "Expected title to be " + expectedTitle);
        assertEquals(productPage.getPrice(),expectedPrice, "Expected price to be " + expectedPrice);
        sleep(2 * 1000);
    }

    @Test(testName = "User can add products to cart test.",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_add_products_to_cart(Product p) {
        p.addProductToCart();
        assertTrue(homePage.getHeader().validateShoppingBadgeIconIsDisplayed(), "Expected shopping badge icon to be displayed");
        sleep(2 * 1000);
    }

    @Test(testName = "Add product to wishlist test.",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_products_to_wishlist(Product p) {
        p.addProductToWishlist();
        assertTrue(homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to be displayed");
        sleep(1000);
    }

    @Test(testName = "Remove product from wishlist test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void user_can_remove_products_from_wishlist(Product p) {
        p.addProductToWishlist();
        p.removeProductFromWishlist();
        assertFalse(homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to disappear");
        sleep(1000);
    }
}
