package org.fasttrackit.cart;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.products.Product;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProductsInCartPage extends CartPage {
    private final SelenideElement productLinkCart;
    private final SelenideElement container;
    private final SelenideElement plusButton;
    private final SelenideElement minusButton;
    private final SelenideElement trashButton;
    private final SelenideElement continueShoppingButton;
    private final SelenideElement checkoutButton;

    public ProductsInCartPage(Product product) {
        this.productLinkCart = $(String.format("#item_%s_title_link", product.getProductId()));
        this.container = productLinkCart.parent().parent();
        this.plusButton = $(".fa-plus-circle");
        this.minusButton = $(".fa-minus-circle");
        this.trashButton = $(".fa-trash ");
        this.continueShoppingButton = $(".btn-danger");
        this.checkoutButton = $(".btn-success");
    }

    public void removeProductFromCart() {
        this.trashButton.click();
        sleep(150);
    }

    /**Getters
     */
    public String getProductCartTitle() {
        return productLinkCart.text();
    }

    public boolean getPlusButton() {
        return this.plusButton.exists() && this.plusButton.isDisplayed();
    }

    public boolean getMinusButton() {
        return this.minusButton.exists() && this.minusButton.isDisplayed();
    }
    public boolean validateProductCartTitleIsDisplayed () {
        return this.productLinkCart.exists() && this.productLinkCart.isDisplayed();
    }
    public boolean getTrashButton () {
        return this.trashButton.exists() && this.trashButton.isDisplayed();
    }
    public boolean getContinueShoppingButton () {
        return this.continueShoppingButton.exists() && this.continueShoppingButton.isDisplayed();
    }
    public boolean getCheckoutButton () {
        return this.checkoutButton.exists() && this.checkoutButton.isDisplayed();
    }


    @Override
    public String toString() {
        return this.productLinkCart.text();
    }
}

