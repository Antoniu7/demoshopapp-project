package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;

import static com.codeborne.selenide.Selenide.$;

public class SearchPage extends MainPage {
    private final SelenideElement productLinkSearch;
    private final SelenideElement cardSearch;



    public String getProductLinkSearch() {
        return productLinkSearch.text();
    }

    public SearchPage(Product product) {
        this.productLinkSearch = $(String.format(".card-body [href='#/product/%s']", product.getProductId()));

        this.cardSearch = productLinkSearch.parent().parent();
    }
    @Override
    public String toString() {
        return this.productLinkSearch.text();
    }
}

