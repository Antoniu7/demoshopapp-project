package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WishlistPage extends MainPage {
    private final SelenideElement pageSubtitle;
    private final ElementsCollection productsInWishlist;

    public WishlistPage() {
        open("https://fasttrackit-test.netlify.app/#/wishlist");
        this.pageSubtitle = $(".subheader-container .text-muted");
        this.productsInWishlist = $$(".col .card");

    }
    public void validateThatTheWishlistIsDisplayed() {
        System.out.println(" Verify that the Wishlist page is displayed ");
    }

    public String getPageSubtitle() {
        return pageSubtitle.text();
    }

    public boolean validateProductsInWishlistAreDisplayed () {
        for (SelenideElement product : this.productsInWishlist) {
            if (!product.isDisplayed()) {
                return false;
            }
        }
        return true;
    }

}
