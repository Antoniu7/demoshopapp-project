package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CartPage extends MainPage{

    private final SelenideElement cartPageTitle;
    private final SelenideElement cartPageMessage;

    public CartPage() {
        open("https://fasttrackit-test.netlify.app/#/cart");
        this.cartPageTitle = $(".subheader-container .text-muted");
        this.cartPageMessage = $ (".text-center.container");
    }
    public String getCartPageTitle() {
        return cartPageTitle.text();
    }

    public String getCartPageMessage() {
        return cartPageMessage.text();
    }
}


