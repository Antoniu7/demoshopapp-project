package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");
    private final SelenideElement shoppingBadgeIcon = $(".fa-shopping-cart.fa-w-18~span");
    private final SelenideElement wishlistBadgeIcon = $(".fa-heart~span");
    private final SelenideElement shoppingCartIconUrl = $ (".navbar-collapse [data-icon=shopping-cart]");
    private final SelenideElement wishlistIconUrl = $(".navbar-collapse [data-icon=heart]");
    private final String greetingsMessage;
    private final SelenideElement signInButton = $(".fa-sign-in-alt");
    private final SelenideElement signOutButton = $(".fa-sign-out-alt");
    private final SelenideElement mandatoryUsernameValidationMessage = $(".error");

    public Header() {
        this.greetingsMessage = "Hello guest!";
    }

    public Header(String user) {
        this.greetingsMessage = "Hi " + user + "!";
    }

    /**
     * Getters
     */

        public SelenideElement getLogoIconUrl() {
        return this.logoIconUrl;
    }

    public SelenideElement getShoppingCartIconUrl() {
        return this.shoppingCartIconUrl;
    }

    public SelenideElement getWishlistIconUrl() {
        return this.wishlistIconUrl;
    }

    public String getGreetingsMessage() {
        return this.greetingsMessage;
    }

    public SelenideElement getSignInButton() {
        return this.signInButton;
    }

    /**
     * Actions
     */
    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the login button.");
        this.signInButton.click();
    }

    public void clickOnShoppingCartIcon() {
        this.shoppingCartIconUrl.click();
    }

    public void clickOnTheLogoutButton() {
        this.signOutButton.click();
    }

    public void clickOnTheWishlistButton() {
        System.out.println("Clicked on the Wishlist button ");
        this.wishlistIconUrl.click();
    }

    public void clickOnTheLogoButton() {
        this.logoIconUrl.click();
        System.out.println("Return to the home page ");
    }

    /**
     * Validators
     */
    public boolean validateUsernameErrorMessageIsDisplayed(){
        return this.mandatoryUsernameValidationMessage.isDisplayed();
    }

    public boolean validateShoppingBadgeIconIsDisplayed() {
        return shoppingBadgeIcon.exists() && shoppingBadgeIcon.isDisplayed();
    }

    public boolean validateWishlistBadgeIconIsDisplayed() {
        return wishlistBadgeIcon.exists() && wishlistBadgeIcon.isDisplayed();

    }

    public boolean validateLogoIconIsDisplayed () {
        return logoIconUrl.exists() && logoIconUrl.isDisplayed();
    }
    public boolean validateShoppingCartIconIsDisplayed () {
        return shoppingCartIconUrl.exists() && shoppingCartIconUrl.isDisplayed();
    }
    public boolean validateWishlistIconIsDisplayed () {
        return wishlistIconUrl.exists() && wishlistIconUrl.isDisplayed();
    }
    public String validateGreetingsMessageIsDisplayed (){
        return greetingsMessage;
    }
    public boolean validateSignInIconIsDisplayed () {
        return signInButton.exists() && signInButton.isDisplayed();
    }

    public String getShoppingBadgeIcon() {
        return this.shoppingBadgeIcon.text();
    }
}
