# Final project for Demo shop application testing

## Short Description
This application is responsible for testing the main functionalities of the Demo Shop
 - Checkout
 - Product Listing
 - Search
 - Login
 - Add to wishlist


# Demo Shop Test Automation Framework
A brief presentation of my project

## This is the final project for Popa Antoniu, within the FastTrackIt Test Automation course.

### Software engineer: _Popa Antoniu_

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framework
    - PageObject Models

### How to run the tests
`git clone https://gitlab.com/Antoniu7/demoshopapp-project.git`

Execute the following commands to:
### Execute all tests
- `mvn clean tests`
### Generate report
- `mvn allure:report`
> Open and present report
- `mvn allure:serve`

### Page Objects
    - Body (Footer, Header, Modal)
    - Cart (ProductsInCartPage)
    - Pages (CartPage, MainPage, Page, ProductDetailsPage, SearchPage, WishlistPage)
    - Products (Product, ProductExpectedResults)

### Test Implemented
| Test Name                                              | Execution Status | Date       |
|--------------------------------------------------------|------------------|------------|
| Verify cart page title is displayed                    | **PASSED**       | 18.03.2023 |
| Verify cart page message is displayed                  | **PASSED**       | 18.03.2023 |
| User can't login on page without password              | **PASSED**       | 18.03.2023 |
| Modal components are displayed and modal can be closed | **PASSED**       | 19.03.2023 |
| User can login on the demo app                         | **PASSED**       | 19.03.2023 |
| User can't login with locked account                   | **PASSED**       | 19.03.2023 |
| User can't login on the page without password          | **PASSED**       | 27.03.2023 |
| Validate message mandatory username field              | **PASSED**       | 27.03.2023 |
| Verify cart page elements                              | **PASSED**       | 27.03.2023 |
| User can add products to cart                          | **PASSED**       | 28.03.2023 |
| User can remove products from cart                     | **PASSED**       | 28.03.2023 |
| User can click on a product                            | **PASSED**       | 29.03.2023 |
| User can add products to cart                          | **PASSED**       | 29.03.2023 |
| User can add products to wishlist                      | **PASSED**       | 29.03.2023 |
| Verify demo shop app title                             | **PASSED**       | 02.04.2023 |
| Verify demo shop footer build date details             | **PASSED**       | 02.04.2023 |
| Verify demo shop footer contains question icon         | **PASSED**       | 02.04.2023 |
| Verify demo shop footer contains reset icon            | **PASSED**       | 02.04.2023 |
| Verify the wishlist page title is displayed            | **PASSED**       | 02.04.2023 |
| Verify the wishlist page contain products              | **PASSED**       | 02.04.2023 |
| User can't login on the page without username          | **PASSED**       | 07.04.2023 |
| User can't login on the page with wrong username       | **PASSED**       | 07.04.2023 |
| User can't login on the page with wrong password       | **PASSED**       | 07.04.2023 |
| Verify sort field is displayed                         | **PASSED**       | 07.04.2023 |
| Verify search button is displayed                      | **PASSED**       | 08.04.2023 |
| Verify header greetings message                        | **PASSED**       | 08.04.2023 |
| Verify header contains sign in icon                    | **PASSED**       | 08.04.2023 |
| Verify header contains wishlist icon                   | **PASSED**       | 08.04.2023 |
| Verify header contains shopping cart icon              | **PASSED**       | 08.04.2023 |
| Verify header contains logo icon                       | **PASSED**       | 08.04.2023 |
| User can remove products from wishlist                 | **PASSED**       | 09.04.2023 |
| User can add the same product to cart several times    | **PASSED**       | 09.04.2023 |
| User can remove product from wishlist                  | **PASSED**       | 09.04.2023 |


- Reporting is also available in the Allure-results folder
    - tests are executed using maven


### Tests definition
| Test Description | Test Status | Test Date Implemented |

